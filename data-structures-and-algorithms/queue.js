let students = [];

function print() {
	return students;
};


function enqueue(student) {
	students[students.length] = student
 	return print();
};


function dequeue() {
	for (let i = 0; i < students.length - 1; i++) {
		students[i] = students[i + 1];
	};

	students.length--;
	return print();
};


function front() {
 	return students[0];
};


function size() {
 	return students.length;
};


function isEmpty() {
	if (students.length == 0) {
		return true;
	} else {
		return false;
	}
};


module.exports = {
	print,
	enqueue,
	dequeue,
	front,
	size,
	isEmpty
};