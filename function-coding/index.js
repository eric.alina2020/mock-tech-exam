function countLetter(letter, sentence) {
    let count = 0;

    // Checks whether the letter is a single character.
    if (letter.length == 1) {
        // If letter is a single character, the function counts how many times a letter has occurred in a given sentence then return count.
        for (let i = 0; i < sentence.length; i++) {
            if (letter == sentence[i]) {
                count++;
            }
        };
        return count;
     // If letter is invalid, it returns 'undefined'.
    } else {
        return undefined;
    }; 
};

function isIsogram(text) {

    // makes each characters of the text in lowercase and makes them an array of characters, assigned to the variable charsArray...
    let charsArray = text.toLowerCase().split('');

    for (let i = 0; i < charsArray.length; i++) {
        // If the function finds a repeating letter, return false...
        if (charsArray[i] === charsArray[i + 1]) {
            return false;
        };
    };
    // ...otherwise, return true.
    return true;
};

function purchase(age, price) {
    // Return undefined for people aged below 13.
    if (age < 13) {
        return undefined;
    // Return the discounted price (rounded off) for students aged 13 to 21 and senior citizens. (20% discount)
    } else if (age >= 13 && age <= 21 || age > 64) {
        return (price * 0.8).toFixed(2).toString();
    // Return the rounded off price for people aged 22 to 64.
    } else {
        return price.toFixed(2).toString();
    };
};


function findHotCategories(items) {
    let hotCategories = [];

    // returns an array of objects with zero stocks
    const zeroStocks = items.filter((item) => {
        if (item.stocks == 0) {
            return item;
        }
    });

    // returns an array of values for category key
    const categories = zeroStocks.map((item) => {
        return item.category;
    });

    // reiterates and checks for duplicate categories, and then pushed into the hotCategories array
    for (let i = 0; i < categories.length; i++) {
        if (categories[i] !== categories[i + 1]) {
            hotCategories.push(categories[i]);
        };
    };

    return hotCategories;

};

function findFlyingVoters(candidateA, candidateB) {
    // Find voters who voted for both candidate A and candidate B.
    let flyingVoters = candidateA.filter(voter => candidateB.includes(voter));

    return flyingVoters;
};

module.exports = {
    countLetter,
    isIsogram,
    purchase,
    findHotCategories,
    findFlyingVoters
};